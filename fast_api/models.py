from sqlalchemy import Column, String, Integer, Text

from database import Base


class Recipe(Base):
    __tablename__ = "Recipes"
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True)
    views = Column(Integer, default=0)
    cook_time = Column(Integer)
    ingredients = Column(String)
    description = Column(Text)
# TODO В данном проекте намного удобнее использовать две таблицы, соединённые с помощью поля "внешний ключ": Receipt и
#  Ingredient. В таблице Ingredient храним ингридиенты рецептов, одна запись - один ингридиент, состав полей модели
#  примерно такой:
# - внешний ключ на таблицу Рецепт
# - название ингридиента
# - количество ингридиента
# - единица измерения
# - примечание (описание)
# Можно добавить ещё полей по желанию. А вот дублировать "время приготовления"
