from pydantic import BaseModel


class BaseRecipe(BaseModel):
    title: str
    cook_time: int
    ingredients: str
    description: str


class RecipeIn(BaseRecipe):
    ...


class RecipeOut(BaseRecipe):
    id: int
    views: int

    class Config:
        orm_mode = True


class RecipeInDB(RecipeIn):
    views: int = 0
