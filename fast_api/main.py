from typing import List

from fastapi import FastAPI, Depends, HTTPException, Path
from sqlalchemy.future import select
from sqlalchemy.orm import Session

import models
import schemas
from database import engine, SessionLocal

app = FastAPI()


@app.on_event("startup")
async def shutdown():
    async with engine.begin() as conn:
        await conn.run_sync(models.Base.metadata.create_all)


@app.on_event("shutdown")
async def shutdown():
    await SessionLocal.close()
    await engine.dispose()


@app.post('/recipes/', response_model=schemas.RecipeOut)
async def create_recipe(recipe: schemas.RecipeIn) -> models.Recipe:
    new_recipe = models.Recipe(**recipe.dict())
    async with SessionLocal.begin():
        SessionLocal.add(new_recipe)
    return new_recipe


@app.get('/recipes/', response_model=List[schemas.RecipeOut])
async def get_recipes() -> List[models.Recipe]:
    recipes = await SessionLocal.execute(select(models.Recipe))
    return recipes.scalars().all()


@app.get('/recipes/{recipe_id}', response_model=schemas.RecipeOut)
async def get_recipe(recipe_id: int) -> schemas.RecipeOut:
    recipe = await SessionLocal.execute(select(models.Recipe).filter(models.Recipe.id == recipe_id))
    recipe = recipe.scalar()

    if recipe is None:
        raise HTTPException(status_code=404, detail="Recipe not found")

    recipe.views += 1

    # Обновляем значение в базе данных
    await SessionLocal.commit()

    return recipe
